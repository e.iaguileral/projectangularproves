import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {UserformService} from "../../shared/services/userform.service";
import {of} from "rxjs";

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let userform: UserformService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports:[HttpClientTestingModule,ReactiveFormsModule],
      providers:[HttpClient,UserformService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    userform = TestBed.inject(UserformService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('register proves', () => {
    const res = [{nom: 'prueba2@ies-sabadell.cat', password: 'Password123'}];
    spyOn(userform, 'setUser').and.returnValue(of(res));
    component.regForm.patchValue({nom: 'prueba2@ies-sabadell.cat', password: 'Password123'});
    component.register();
    fixture.detectChanges();
    //expect(component.message).toEqual("Guardat!!!");
    console.log(component.message);
    expect(component.message).toEqual(jasmine.objectContaining(res));
  });
});
