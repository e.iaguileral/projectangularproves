import { Component } from '@angular/core';
import {FormBuilder,FormControl, FormGroup, Validators} from "@angular/forms";
import {UserformService} from "../../shared/services/userform.service";
import {emailValidator, passwordValidator} from "./loginvalidator.directive";
import {User} from "../../shared/classes/user";
import {LoginService} from "../../shared/services/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  constructor (private userform:UserformService,private loginservice:LoginService) { }
  loginForm !: FormGroup;
  users : User[]=[];
  message! : string;
  ngOnInit(): void {
    this.loginForm = new FormGroup( {
      nom : new FormControl('',
        [Validators.required,emailValidator(), Validators.minLength(7),
          Validators.required]),
      password : new FormControl('',
        [Validators.required,passwordValidator(),Validators.minLength(8)])
    })
  }

  /**
   * Aquesta funció utilitza la funció de loguejar un usuari del servei amb l'usuari i la contrasenya guardats a "loginForm".
   * Si la funció de loguejar no ha retornat un usuari, canvia el "message" per "login incorrecte.".
   * Si la funció sí que ha retornat un usuari, guarda l'usuari per poder utilitzar-ho en altres components i actualitza
   * "message" amb un missatge per confirmar que l'usuari ha sigut loguejat correctament.
   *
   * @return És un void, però el missatge que retorna el registra canvia depenen de la resposta de la funció de loguejar.
   */
  login() {
    this.userform.getLogin((this.loginForm))
      .subscribe(res => {
        console.log(res);
        if (res.length == 0) {
          this.users = [];
          this.message = "Login incorrecte.";
        } else {
          this.users = res;
          this.loginservice.updateLoginData(this.users[0].nom);
          this.message = "L'usuari " + this.users[0].nom
            + " s'ha logejat de manera correcte! ! ! ";
        }
      })
  }
}
