import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {UserformService} from "../../shared/services/userform.service";
import {of} from "rxjs";

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let userform: UserformService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports:[HttpClientTestingModule,ReactiveFormsModule],
      providers:[HttpClient,UserformService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    userform = TestBed.inject(UserformService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('login proves', () => {
    const res = [{nom: 'iker@ies-sabadell.cat', password: 'Password123'}];
    spyOn(userform, 'getLogin').and.returnValue(of(res));
    component.loginForm.patchValue({nom: 'iker@ies-sabadell.cat', password: 'Password123'});
    component.login();
    fixture.detectChanges();
    expect(component.message).toEqual("L'usuari iker@ies-sabadell.cat s'ha logejat de manera correcte! ! ! ");
  });
});
