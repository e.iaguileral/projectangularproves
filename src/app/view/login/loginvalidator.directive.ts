import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, ValidatorFn} from "@angular/forms";

@Directive({
  selector: ' [appValidarPassword]',
  providers: [{provide: NG_VALIDATORS,
    useExisting: LoginvalidatorDirective, multi: true
  }]
})
export class LoginvalidatorDirective {
  @Input('appName') passwordStrength = false;
  @Input('appName') emailStrength = false;

  validate(control: AbstractControl): ValidationErrors | null {
    return this.passwordStrength ? passwordValidator()(control)
      : null;
  }

  constructor() {
  }
}
export function passwordValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control. value;
    if (!value) {
      return null;
    }
    const hasUpperCase : boolean = /[A-Z]+/.test(value);
    const hasLowerCase : boolean = /[a-z]+/. test(value);
    const hasNumeric : boolean = /[0-9]+/.test(value);
    const specialCaracter : boolean = /[@#]/.test(value);
    const passwordValid : boolean = hasUpperCase && hasLowerCase && hasNumeric && !specialCaracter;
    return !passwordValid ? {passwordStrength: true}: null;
  };
}
export function emailValidator():ValidatorFn{
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (!value){
      return null;
    }
    const isemail : boolean = /@ies-sabadell\.cat$/.test(value);
    return !isemail ? {emailStrength: true}: null;
  }
}
