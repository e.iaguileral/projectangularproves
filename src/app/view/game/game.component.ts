import {Component, HostListener} from '@angular/core';
import {User} from "../../shared/classes/user";
import {LoginService} from "../../shared/services/login.service";
import {UserformService} from "../../shared/services/userform.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {emailValidator, passwordValidator} from "../login/loginvalidator.directive";
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrl: './game.component.css'
})

export class GameComponent {
  block:boolean = true;
  idInterval?:NodeJS.Timeout;
  spd:number = 300;
  capturades:number = 0;
  fallos:number = 0;
  gridarray:string[][] =[];
  logueado!:string;
  record!:number;
  finish:boolean = false;
  comprovacio!:string;
  usrForm:FormGroup = new FormGroup( {
    nom : new FormControl(this.logueado),
    punts : new FormControl(this.record)
  })

  /**
   * Aquesta funció agafa tots els usuaris guardats a la base de dades utilitzant la funció del servei.
   * Després, comprova si l'usuari loguejat existeix entre els usuaris i, si existeix, actualitza el rècord amb els punts
   * de l'usuari.
   *
   * @return És un void, però l'objectiu és canviar la variable "record".
   */
  getRecord(){
    this.userform.getAllUsers().subscribe( res =>{
      for(let u of res){
        if(u.nom === this.logueado){
          this.record = u.punts;
        }
      }
    });
  }
  constructor(private loginservice:LoginService, private userform:UserformService) {
  }

  /**
   * Funció que s'activa a l'inici. Només serveix per guardar l'usuari loguejat i per executar la funció d'actualitzar la variable "record".
   */
  ngOnInit(){
    this.logueado = this.loginservice.userlogin.getValue();
    this.getRecord();

  }

  /**
   * Aquesta funció executa la funció per actualitzar els punts dels usuaris de la base de dades.
   * S'utilitza la variable "usrForm" amb l'usuari i els nous punts.
   */
  updateRecord() {
    this.userform.updateUser((this.usrForm))
      .subscribe(res => {
        console.log(res);
        this.comprovacio = res;
      });
  }

  /**
   * Aquesta funció s'executa quan el jugador pressiona el botó de començar a jugar.
   * El principal que fa és canviar algunes variables que defineixen l'estat del joc per a reiniciar-lo.
   * També genera una activitat al principi, i activa l'interval de temps per activar la funció per moure
   * les activitats constantment.
   *
   * @see {@link generarActividad}
   *
   * @see {@link https://tsdoc.org/}
   *
   * @return És un void, però canvia les variables integers de "spd", "fallos", "capturades" i els booleans de "finish", "block";
   */
  comenzarPartida(){
    this.gridarray = [
      ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'eloi', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall']
    ];
    this.spd = 300;
    this.generarActividad();
    clearInterval(this.idInterval);
    this.idInterval = setInterval(() => this.movimientoActividad(),this.spd);
    this.fallos = 0;
    this.capturades = 0;
    this.block = false;
    this.finish = false;

  }

  @HostListener('window:keydown', ['$event'])
  /**
   * Aquesta funció bàsicament mou el personatge a l'esquerra o dreta, depenen de la tecla premuda.
   * El personatge es mou sempre que hi hagi un espai buit.
   * Nomès s'executa la funció si l'usuari no està bloquejat.
   * Abans de tot guarda en les variables "f" i "c" les coordenades del personatge, això recorrent
   * la matriu del joc.
   *
   * @param event - guarda el event de pressionar una tecla.
   *
   * @return És un void, però canvia la matriu del joc principal quan mou el personatge.
   */
  handleKeyDown(event: KeyboardEvent){
    if(!this.block){
      let f:number = 0;
      let c:number = 0;
      for(let i=0;i<this.gridarray.length;i++){
        for(let k=0;k<this.gridarray[0].length;k++){
          if(this.gridarray[i][k] === 'eloi'){
            f = i;
            c = k;
          }
        }
      }
      switch (event.code){
        case 'ArrowLeft':
          if(this.gridarray[f][c-1] === 'dot-unhit'){
            this.gridarray[f][c] = 'dot-unhit';
            this.gridarray[f][c-1] = 'eloi';
          }
          break;
        case 'ArrowRight':
          if(this.gridarray[f][c+1] === 'dot-unhit'){
            this.gridarray[f][c] = 'dot-unhit';
            this.gridarray[f][c+1] = 'eloi';
          }
          break;
      }
    }
  }

  /**
   * Aquesta funció generà una activitat en un punt aleatori a la segona fila de la matriu.
   * Si la generació falla perquè s'ha generat en un lloc que no toca es torna a generar fins que termina.
   *
   * @return És un void, però actualitza la matriu amb la nova activitat.
   */
  generarActividad() {
    let generar:boolean = true;
    while(generar){
      const randomPos:number = Math.floor(Math.random() * (this.gridarray[0].length-1)+1);
      if(this.gridarray[1][randomPos] === 'dot-unhit'){
        this.gridarray[1][randomPos] = "act"
        generar = false;
      }
    }

  }

  /**
   * Aquesta funció és realment llarga.
   * Primer troba una activitat en la matriu i guarda les coordenades, amb l'objectiu de moure-la.
   * Si el lloc on es mou està buit es mou sense problemes.
   * Si el lloc on es mou és una paret, es destrueix i suma un error.
   * En aquest cas, si s'obté 10 errors o més, el joc termina canviant totes les variables que defineixen
   * l'estat del joc. A més, guarda l'usuari i els punts, i actualitza el rècord.
   * Si el lloc on es mou l'activitat és el jugador, es destrueix, suma un punt i crida a la funció per generar
   * una nova activitat.
   * En aquest cas, cada 20 punts obtinguts, la velocitat de les activitats augmenta, canviant l'interval per moure les activitats.
   *
   * @see {@link generarActividad}
   * @see {@link updateRecord}
   * @see {@link getRecord}
   */
  movimientoActividad(){
    let f:number = 100;
    let c:number = 100;
    for(let i=0;i<this.gridarray.length;i++){
      for(let k=0;k<this.gridarray[0].length;k++){
        if(this.gridarray[i][k] === 'act'){
          f = i;
          c = k;
        }
      }
    }

    if(this.gridarray[f+1][c] === "dot-unhit"){
      this.gridarray[f+1][c] = "act";
      this.gridarray[f][c] = "dot-unhit";
    }
    else if(this.gridarray[f+1][c] === "wall"){
      this.gridarray[f][c] = "dot-unhit";
      this.fallos++;
      if(this.fallos >= 10){
        clearInterval(this.idInterval);
        this.finish = true;
        this.block = true;
        if(this.record < this.capturades){
          this.record = this.capturades;
          this.usrForm = new FormGroup( {
            nom : new FormControl(this.logueado),
            punts : new FormControl(this.record)
          })
          this.updateRecord();
          this.getRecord();
        }
      }else{
        this.generarActividad();
      }
    }
    else if(this.gridarray[f+1][c] === "eloi"){
      this.gridarray[f][c] = "dot-unhit";
      this.capturades++;
      this.generarActividad();
      if(this.capturades%20==0){
        this.spd -= 100;
        if(this.spd == 0){
          this.spd = 100;
        }
        clearInterval(this.idInterval);
        this.idInterval = setInterval(() => this.movimientoActividad(),this.spd);
      }
    }
  }
}
