import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameComponent } from './game.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {UserformService} from "../../shared/services/userform.service";
import {of} from "rxjs";

describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;
  let userform: UserformService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GameComponent],
      imports:[HttpClientTestingModule,ReactiveFormsModule],
      providers:[HttpClient,UserformService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    userform = TestBed.inject(UserformService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('update proves', () => {
    const res = [{nom: 'prueba@ies-sabadell.cat', punts: 300}];
    spyOn(userform, 'updateUser').and.returnValue(of(res));
    component.usrForm.patchValue({nom: 'prueba@ies-sabadell.cat', punts: 300});
    component.updateRecord();
    fixture.detectChanges();
    console.log(component.comprovacio);
    expect(component.comprovacio).toEqual(jasmine.objectContaining(res));
  });
});
