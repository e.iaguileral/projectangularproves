import {Component, ElementRef, ViewChild} from '@angular/core';
import {UserformService} from "../../shared/services/userform.service";
import {User} from "../../shared/classes/user";
import jsPDF from 'jspdf';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from "html-to-pdfmake";

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrl: './ranking.component.css'
})
export class RankingComponent {
  constructor(public userform:UserformService) {
  }
  user !:User[];

  /**
   * Aquesta funció agafa la funció del servei per veure tots els usuaris existents
   * a la base de dades.
   *
   * @returns És un void, però guarda una llista dels usuaris en la variable "user".
   *
   */
  getUsers(){
    this.userform.getAllUsers().subscribe( res =>{
      if(res.length == 0){
        this.user=[];
      }else{
        this.user = res;
      }
    });
  }

  @ViewChild('pdfTable') pdfTable!: ElementRef;
  /**
   * Aquesta funció genera un PDF amb les dades guardades de "PDFTable", que és la variable que té la taula
   * amb els usuaris de la variable "user".
  */
  GetPDF(){
    const doc = new jsPDF();
    const pdfTable = this.pdfTable.nativeElement;
    var html = htmlToPdfmake(pdfTable.innerHTML);
    const documentDefinition = {content:html};
    pdfMake.createPdf(documentDefinition).open();
  }
}
