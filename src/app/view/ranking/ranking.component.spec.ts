import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RankingComponent } from './ranking.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ReactiveFormsModule} from "@angular/forms";
import {UserformService} from "../../shared/services/userform.service";
import {of} from "rxjs";
import {HttpClient} from "@angular/common/http";

describe('RankingComponent', () => {
  let component: RankingComponent;
  let fixture: ComponentFixture<RankingComponent>;
  let userform: UserformService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RankingComponent],
      imports:[HttpClientTestingModule, ReactiveFormsModule],
      providers: [UserformService,HttpClient]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RankingComponent);
    component = fixture.componentInstance;
    userform = TestBed.inject(UserformService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('get ranking', () => {
    const res = [{nom: 'prueba@ies-sabadell.cat', punts: 119}];
    spyOn(userform, 'getAllUsers').and.returnValue(of(res));
    component.getUsers();
    fixture.detectChanges();
    expect(component.user).toContain(jasmine.objectContaining({nom: 'prueba@ies-sabadell.cat', punts: 119}));
  })

});
