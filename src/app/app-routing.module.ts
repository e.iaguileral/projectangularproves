import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StartpageComponent} from "./view/startpage/startpage.component";
import {GameComponent} from "./view/game/game.component";
import {LoginComponent} from "./view/login/login.component";
import {RegisterComponent} from "./view/register/register.component";
import {RankingComponent} from "./view/ranking/ranking.component";
import {HelpComponent} from "./view/help/help.component";
const routes: Routes = [
  {path:'start', component:StartpageComponent},
  {path:'game', component:GameComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'ranking',component:RankingComponent},
  {path:'help',component:HelpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
