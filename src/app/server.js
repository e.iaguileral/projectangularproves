const express= require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const {Observable} = require("rxjs");

app.use(cors())

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.listen(3000, function () {
  console.log('Node app is running on port 3000');
});
module.exports = app;

const client = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'project';
const collection = 'users';

app.get('/users', async (req,
                         res) => {
    let client2;
    try {
      client2 = await client.connect(url);
      const db = client2.db(dbName);
      const users =
        await db.collection(collection).find().sort({"punts":-1}).toArray();
      res.json(users);
    } catch (err) {
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally {
      if (client2) {
        client2.close();
      }
    }
  }
);
app.get('/users/login',
  async (req,
         res) => {
    let client2;
    let nom = req.query.nom;
    let password = req.query.password;
    try {
      client2 = await client.connect(url);
      const db = client2.db(dbName);
      const users = await db.collection(collection).find({
        'nom': {$eq:nom},
        'password': {$eq:password}
      }).toArray();
      res.json(users);
    } catch (err) {
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally {
      if (client2) {
        client2.close();
      }
    }
  }
);

app.post('/user/insert',
  async (req,
         res) => {
    let client2;
    let nom = req.body.nom;
    let password = req.body.password;
    try {
      client2 = await client.connect(url);
      const db = client2.db(dbName);
      const users = await db.collection(collection).find({
        'nom': {$eq:nom}
      }).toArray();
      if(users.length == 0){
        let id = (await db.collection(collection).find().toArray()).length + 1;
        const usuaris = await db.collection(collection).
        insertOne({ 'id': id,'nom': nom,'password': password,'punts': 0});
        res.json("Guardat!!!");
      }else{
        res.json("L'usuari ja existeix.");
      }

    } catch (err) {
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally {
      if (client2) {
        client2.close();
      }
    }
  }
);

app.post('/user/update',
  async (req,
         res) => {
    let client2;
    let nom = req.body.nom;
    let punts = req.body.punts;
    console.log(nom);
    console.log(punts);
    try {
      client2 = await client.connect(url);
      const db = client2.db(dbName);
      const usuaris = await db.collection(collection).updateOne({'nom': nom}, {$set: {'punts': punts}});
      const users = await db.collection(collection).find({
        'nom': {$eq: nom}
      }).toArray();
      res.json(users);
    } catch (err) {
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally {
      if (client2) {
        client2.close();
      }
    }
  }
);


