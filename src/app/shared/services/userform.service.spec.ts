import { TestBed } from '@angular/core/testing';

import { UserformService } from './userform.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {HttpClient} from "@angular/common/http";
import {FormControl, FormGroup} from "@angular/forms";

describe('UserformService', () => {
  let service: UserformService;
  let HttpClientSpy: jasmine.SpyObj<HttpClient>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    HttpClientSpy = jasmine.createSpyObj('HttpClient', ['get','post']);
    service = new UserformService(HttpClientSpy);
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      providers: [UserformService]
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(UserformService);
  });

  it('no furros', () => {
    expect(service).toBeTruthy();

  });

  it('update url and metodo', () => {
    let usrForm!:FormGroup;
    usrForm = new FormGroup( {
      nom : new FormControl("iker@ies-sabadell.cat"),
      punts : new FormControl(10)
    })
    service.updateUser(usrForm).subscribe({});
    const req = httpTestingController.expectOne(service.REST_API + "/user/update");
    expect(req.request.url).toBe(service.REST_API + "/user/update")
    expect(req.request.method).toEqual('POST');
  });

  it('update url and metodo', () => {
    let usrForm!:FormGroup;
    usrForm = new FormGroup( {
      nom : new FormControl("iker@ies-sabadell.cat"),
      punts : new FormControl(10)
    })
    service.updateUser(usrForm).subscribe({});
    const req = httpTestingController.expectOne(service.REST_API + "/user/update");
    expect(req.request.url).toBe(service.REST_API + "/user/update")
    expect(req.request.method).toEqual('POST');
  });

  it('getusers url and metodo', () => {
    service.getAllUsers().subscribe({});
    const req = httpTestingController.expectOne(service.REST_API + "/users");
    expect(req.request.url).toBe(service.REST_API + "/users")
    expect(req.request.method).toEqual('GET');
  });

  it('login url and metodo', () => {
    let usrForm!:FormGroup;
    usrForm = new FormGroup( {
      nom : new FormControl("iker@ies-sabadell.cat"),
      password : new FormControl("Password123")
    })
    service.getLogin(usrForm).subscribe({});
    const req = httpTestingController.expectOne(service.REST_API + "/users/login?nom=iker@ies-sabadell.cat&password=Password123");
    expect(req.request.url).toBe(service.REST_API + "/users/login")
    expect(req.request.method).toEqual('GET');
  });

  it('register url and metodo', () => {
    let usrForm!:FormGroup;
    usrForm = new FormGroup( {
      nom : new FormControl("iker@ies-sabadell.cat"),
      password : new FormControl("Password123")
    })
    service.setUser(usrForm).subscribe({});
    const req = httpTestingController.expectOne(service.REST_API + "/user/insert");
    expect(req.request.url).toBe(service.REST_API + "/user/insert")
    expect(req.request.method).toEqual('POST');
  });

  /**
   * it('Prova Comprova número',() =>{
   *     component.myForm.controls['num1'].setValue(35);
   *     fixture.detectChanges();
   *     expect(component.myForm.controls['num1'].value).toBe(35);
   *   });
   *
   *   it('Prova Comprova funció',()=>{
   *     component.myForm.controls['num1'].setValue(35);
   *     component.myForm.controls['num2'].setValue(22);
   *     component.Calcular();
   *     fixture.detectChanges();
   *     expect(component.dada).toBe(57);
   *   });
   *
   *   it('Prova Element del DOM',()=>{
   *     component.myForm.controls['num1'].setValue(35);
   *     component.myForm.controls['num2'].setValue(22);
   *     component.Calcular();
   *     fixture.detectChanges();
   *     const compiled = fixture.debugElement.nativeElement;
   *     expect(compiled.querySelector('span').textContent).toBe("El resultat és: 57");
   *   });
   *
   *   import mult from 'paquetikeraguilera';
   *   this.dada = mult(this.myForm.get("num1")?.value,this.myForm.get("num2")?.value);
   *
   *   ng build para hacer la distribución
   *
   *   function mult(num1, num2){
   *     return num1*num2;
   *   }
   *
   *   module.exports = mult
   *
   *   module.exports = {mult: mult}
   *
   *   const paquet = require('paquete')
   *   paquet.mult
   */
});
