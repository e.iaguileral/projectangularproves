import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor() { }
  userlogin: BehaviorSubject<any> = new BehaviorSubject<any>('logout');
  updateLoginData(user:any){
    this.userlogin.next(user);
  }
}
